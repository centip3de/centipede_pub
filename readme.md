**Centipede Vulnerable Web Application**

![Scheme](user/logo.jpg)

Centipede is a vulnerable web application that was created for educational purposes. This application is just like any other vulnerable web application, where you have to find and exploit common vulnerabilities. The application is designed to be a simple blog that has two types of users: users and authors. However, this application was meant to be tested by multiple attackers in a local lab environment, where a lab trainer can point out flaws in the application, so that the lab students (the attackers) can use these hints to compromise the vulnerable system. The trainers can also track the progress of the students.

---

## Lab trainer

As a lab trainer, you 

- provide availability of the service
- track student progress
- manage users
- maintain tasks, posts and comments

---

## Lab users

- This is a competition, so as a student, you have to compete with other students in terms of compromising the system and solving problems
- If you can successfully compromise a vulnerable component or complete a task, you get points
- MitM attacks are allowed. Good to practice, because other lab users generate network traffic

---

## Standalone mode

- Or you can just complete the challenges and get access to the system

---

## Lab admin's interface

![Scheme](admin/2018-07-08-183834_1600x900_scrot.png)


![Scheme](admin/2018-07-08-183947_1600x900_scrot.png)

---

## Lab users' interface

![Scheme](user/2018-07-08-183426_1600x900_scrot.png)


![Scheme](user/2018-07-08-183536_1600x900_scrot.png)

More picture about the app in the user/admin folder.

---

The application is shipped in virtual environment.

Virtual image / ova file can be downloaded [here.](https://drive.google.com/open?id=1JzCm4KMArFo93FAJmq31ZpyPNJrxM56D)

---

For lab admins - [Download](https://bitbucket.org/centip3de/centipede_pub/src/master/master.zip) passwords.

Please write an e-mail to __gerg.sch@gmail.com__ if you would like to be a lab trainer and need the login passwords.